﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Die Aufgabe: 
Schreiben Sie ein Programm mit folgender Ausgabe:
Erste Zahl : 8
Operator (| & ^): &
Zweite Zahl : 12
Geschätzte Zahl : 7
Falsch! 8 & 12 = 8
Noch eine Runde? (J) done
Testen Sie Ihr Umgang mit den Operatoren!
*/
namespace Bit_Opis
{
    class Program
    {        
        
        /// <summary>
        /// Fordert eine UInt Zahl an, bis sie eine bekommen hat.
        /// </summary>
        /// <param name="s">Eingabeaufforderung für den Benutzer</param>
        /// <returns>Irgenwann eine UInt Zahl</returns>
        public static uint Con2Uint(string s)
        {
            uint num = 0;
            bool result = false;
            while (!result)
            {
                Console.Write(s);
                result = UInt32.TryParse(Console.ReadLine(), out num);
            }
            return num;
        }

        static void Main(string[] args)
        {
            string nochmal = "j";
            while (nochmal == "j")
            {
                //Zahlen abfragen

                uint num1 = Con2Uint("Erste Zahl: >> ");
                

                string op = string.Empty;
                do
                {
                    Console.Write("Operator (| & oder ^) >> ");
                    op = Console.ReadLine();
                } while (op != "&" && op != "|" && op != "^");

                uint num2 = Con2Uint("Zweite Zahl: >> ");


                //fehler = true;
                uint num__guessed = Con2Uint("Geschätzte Zahl: >> ");

                //Rechnen
                uint result = 0;
                switch (op)
                {
                    case "|": //Bitweises ODER
                        result = num1 | num2;
                        break;
                    case "&": //Bitweises UND
                        result = num1 & num2;
                        break;
                    case "^": //Bitweises XOR
                        result = num1 ^ num2;
                        break;
                    default:
                        result = 0;
                        break;
                }

                //Vergleichen und Ausgabe
                if (num__guessed == result)
                {
                    Console.WriteLine("He, das war ja richtig!");
                }
                else
                {
                    Console.WriteLine("Nicht ganz. " + num1 + op + num2 + " ergibt: " + result);
                }

                //Wiederholung
                Console.Write("Noch eine Runde? (j) >> ");
                nochmal = Console.ReadLine();
            }
        }
    }
}
