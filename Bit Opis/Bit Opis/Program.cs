﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Die Aufgabe: 
Schreiben Sie ein Programm mit folgender Ausgabe:
Erste Zahl : 8
Operator (| & ^): &
Zweite Zahl : 12
Geschätzte Zahl : 7
Falsch! 8 & 12 = 8
Noch eine Runde? (J) done
Testen Sie Ihr Umgang mit den Operatoren!
*/
namespace Bit_Opis
{
    class Program
    {
        static void Main(string[] args)
        {
            string nochmal = "j";
            while(nochmal == "j")
            {
                //Zahlen abfragen
                Console.Write("Erste Zahl: >> ");
                uint num1 = Convert.ToUInt32(Console.ReadLine());
                Console.Write("Operator (| & oder ^) >> ");
                string OP = Console.ReadLine();
                Console.Write("Zweite Zahl: >> ");
                uint num2 = Convert.ToUInt32(Console.ReadLine());
                Console.Write("Geschätzte Zahl: >> ");
                uint num__guessed = Convert.ToUInt32(Console.ReadLine());

                //Rechnen
                uint result = 0;
                switch (OP)
                {
                    case "|": //Bitweises ODER
                        result = num1 | num2;
                        break;
                    case "&": //Bitweises UND
                        result = num1 & num2;
                        break;
                    case "^": //Bitweises XOR
                        result = num1 ^ num2;
                        break;
                    default:
                        result = 0;
                        break;
                }

                //Vergleichen und Ausgabe
                if (num__guessed == result)
                {
                    Console.WriteLine("He, das war ja richtig!");
                }
                else
                {
                    Console.WriteLine("Nicht ganz. "+num1+OP+num2+" ergibt: " + result);
                }

                
                //Wiederholung
                Console.Write("Noch eine Runde? (j) >> ");
                nochmal = Console.ReadLine();
            }
        }
    }
}
