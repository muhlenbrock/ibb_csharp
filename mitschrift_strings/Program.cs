﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mitschrift_strings
{
    class Program
    {
        static void Main(string[] args)
        {
            //Heute geht es um Strings...
            //Es gibt mehrere Methoden einen String zu bauen:
            string DurchLiteral = "Jo, das kenn ich so.";
            DurchLiteral = DurchLiteral + " Echt jetzt.";  //Da Konkatenieren etwas rechenlastig ist, sollte man StringBuilder verwenden.

            //Mit einem Char-Array geht das übrigens auch...
            char[] Stückchen = { 'H','a','l','l','o',' ','W','e','l','t' };
            string Hallowelt = new string(Stückchen);

            //oder ein Zeichen n mal wiederholt (Yeah, Deko!)
            string Linie = new string('-', 80); //Das könnte zB eine Trennlinie sein
            Console.WriteLine(Linie);

            //Strings haben einen indexer, über den wir Chars aus dem String ziehen können.
            char j = DurchLiteral[0]; //Liefert nun das 'J';

            //Wie ist das nun bei Umlauten?
            string fiesling = "äöü";
            char test = fiesling[1]; //Liefert ein 'ö'. Anders als PhP, das scheitert in der Form.

            //Console.WriteLine(fiesling.Length); Die Länge ist übrigens auch wirklich in Zeichen, nicht in Bytes.

            /*Interessante Funktionen:
             * StartsWith
             * EndsWith
             * IndexOf
             * Contains
             * Substring
             * Split
             * Join
             * .........ggf nachschlagen bei MSDN */

            string padded = Linie.Substring(6).PadLeft(Linie.Substring(6).Length+3, '*').PadRight(Linie.Substring(6).Length + 6, '*');
            Console.WriteLine(padded);
            

            Console.ReadLine();
        }
    }
}
