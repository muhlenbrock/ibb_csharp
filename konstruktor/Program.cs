﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace konstruktor
{
    class Program
    {
        static void Main(string[] args)
        {
            Demo d = new Demo();
            Demo d2 = new Demo("Hans Wurst");
            Console.WriteLine(d2.Name);

            Console.ReadLine();
        }

        class Demo
        {
            public Demo()
            {
                Console.WriteLine("Dagegen!");
            }
            public Demo(string n)
            {
                Console.WriteLine("Demo mit Name!");
                Name = n;
            }

            public string Name { get; set; }
        }
    }
}
