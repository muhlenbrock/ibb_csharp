﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uebung_fahrzeuge
{
    class Program
    {
        static List<Fahrzeug> Fuhrpark = new List<Fahrzeug>();

        static void Main(string[] args)
        {
            do
            {
                char C = MenuChoice();
                switch (C)
                {
                    case 'X': break;
                    case 'N': Add(); break;
                    case 'A': List(); break;
                    case 'L':
                    case 'Ä':
                        int i = ChooseIndex();
                        if ( i > 0)
                        {                            
                            switch (C)
                            {
                                case 'L': Fuhrpark.Remove(Fuhrpark[i - 1]); break;
                                case 'Ä': Fuhrpark[i-1].Change(); break;
                            }
                        }
                        break;
                }
                Console.Write("Programm Beenden? (j/n) >> ");
            } while (Helpers.ReadYesNo() == "n");
        }

        private static char MenuChoice()
        {
            Console.Clear();
            Console.WriteLine("---- Hauptmenü ----");
            Console.WriteLine("(N)eu");
            Console.WriteLine("(Ä)ndern");
            Console.WriteLine("(L)öschen");
            Console.WriteLine("(A)uflistung");
            Console.WriteLine("E(x)it");
            Console.Write("Bitte auswählen: >> ");
            return char.ToUpper(Console.ReadLine()[0]);
        }

        private static void Add()
        {
            char C = '.';
            do {
                Console.Clear();
                Console.WriteLine("---- Neuanlage Fahrzeug ----");
                Console.WriteLine("(A)uto");
                Console.WriteLine("(B)ulldozer");
                Console.WriteLine("(P)anzer");
                Console.WriteLine("Abbru(c)");
                Console.Write("Bitte auswählen: >> ");
                C = char.ToUpper(Console.ReadLine()[0]);
            } while (!"ABCP".Contains(C));

            switch (C)
            {
                case 'A': Fuhrpark.Add(new Auto()); break;
                case 'B': Fuhrpark.Add(new Bulldozer()); break;
                case 'C': Console.WriteLine("Anlage abgebrochen...."); break;
                case 'P': Fuhrpark.Add(new Panzer()); break;
            }
        }

        private static void List()
        {
            Console.Clear();
            if (Fuhrpark.Count == 0) Console.WriteLine("Nur Spinnenweben in der Garage...");
            else
            {
                for (int i = 0; i < Fuhrpark.Count;i++)
                {
                    Console.WriteLine("__________________________________");
                    Console.WriteLine("Fahrzeugnr.: " + (i + 1));
                    Fuhrpark[i].Show();
                }
            }
        }

        private static int ChooseIndex()
        {
            int i = -1;            
            while (i < 0 || i > Fuhrpark.Count )
            {
                Console.Write("Bitte die gewünschte Nummer eingeben. 0 für Abbruch: >> ");
                i = Helpers.ReadNum();
                if (i < 0 || i > Fuhrpark.Count)
                {
                    Console.WriteLine("Diese Fahrzeugnummer gibt es nicht. Bitte erneut versuchen. 0 für Abbruch: >>");
                }
            }
            return i;
        }
    }

    public static class Helpers
    {
        /// <summary>
        /// Liest eine Zahl aus der Konsole, wiederholt die Abfrage im Fehlerfall
        /// </summary>
        /// <returns></returns>
        public static int ReadNum()
        {
            int zahl = 0;
            
            while (!Int32.TryParse(Console.ReadLine(), out zahl))
            {
                Console.WriteLine("Das war keine Zahl. Noch mal bitte");
            }
            return zahl;
        }        

        /// <summary>
        /// Fragt explizit nach j oder n
        /// </summary>
        /// <returns></returns>
        public static string ReadYesNo()
        {            
            string antw = Console.ReadLine();
            while (antw != "j" && antw != "n")
            {
                Console.Write("Ja was denn nun??? ");
                antw = Console.ReadLine();
            }
            return antw;
        }
    }
}
