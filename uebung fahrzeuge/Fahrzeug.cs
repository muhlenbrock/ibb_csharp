﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uebung_fahrzeuge
{
    abstract class Fahrzeug
    {
        public string Hersteller { get; set; }
        public string Name { get; set; }
        protected char ToChange { get; set; }

        public Fahrzeug()
        {
            Console.Write("Hersteller?          >> ");
            Hersteller = Console.ReadLine();
            Console.Write("Name?                >> ");
            Name = Console.ReadLine();
        }

        public virtual void ShowChangeOptions ()
        {
            Console.WriteLine("Was soll geändert werden?");
            Console.WriteLine("(H)ersteller");
            Console.WriteLine("(N)ame");
        }

        public void SetChangeOption()
        {
            ToChange = char.ToUpper(
                            Convert.ToChar(
                                Console.ReadLine()
                                )
                        );
        }

        public virtual void DoChange()
        {
            switch(ToChange)
            {
                case 'H':
                    Console.Write("Hersteller?          >> ");
                    Hersteller = Console.ReadLine();
                    break;
                case 'N':
                    Console.Write("Name?                >> ");
                    Name = Console.ReadLine();
                    break;
            }
        }

        public new string GetType()
        {
            return base.GetType().ToString().Split(new char[] { '.' })[1];
        }
        public void Change()
        {
            Console.Clear();
            Show();
            Console.WriteLine("--------------------");
            ShowChangeOptions();
            SetChangeOption();
            Console.WriteLine("--------------------");
            DoChange();
            Console.WriteLine("--------------------\nÄnderung erfolgreich. Neue Daten:\n------");
            Show();
            Console.WriteLine("--------------------");
        }

        public virtual void Show()
        {
            Console.WriteLine("Typ: " + GetType());
            Console.WriteLine("Hersteller: " + Hersteller);
            Console.WriteLine("Name: " + Name);
        }
    }

    class Auto: Fahrzeug
    {
        public int Luftdruck { get; set; }

        public Auto()
        {
            Console.Write("Luftdruck?           >> ");
            Luftdruck = Helpers.ReadNum();
            
        }

        public override void ShowChangeOptions()
        {
            base.ShowChangeOptions();
            Console.WriteLine("(L)uftdruck");
        }

        public override void DoChange()
        {
            base.DoChange();
            if (ToChange == 'L')
            {
                Console.Write("Luftdruck?           >> ");
                Luftdruck = Helpers.ReadNum();
            }
        }

        public override void Show()
        {
            base.Show();
            Console.WriteLine("Luftdruck: " + Luftdruck);
        }
    }

    abstract class Kettenfahrzeug : Fahrzeug
    {
        public int Kettenlaenge { get; set; }

        public Kettenfahrzeug()
        {
            Console.Write("Kettenlänge?         >> ");
            Kettenlaenge = Helpers.ReadNum();
        }

        public override void ShowChangeOptions()
        {
            base.ShowChangeOptions();
            Console.WriteLine("(K)ettenlänge");
        }

        public override void DoChange()
        {
            base.DoChange();
            if (ToChange == 'K')
            {
                Console.Write("Kettenlänge?         >> ");
                Kettenlaenge = Helpers.ReadNum();
            }
        }

        public override void Show()
        {
            base.Show();
            Console.WriteLine("Kettenlänge: " + Kettenlaenge);
        }
    }
    class Bulldozer : Kettenfahrzeug
    {

    }

    class Panzer : Kettenfahrzeug
    {
        public int NumGeschuetze { get; set; }

        public Panzer()
        {
            Console.Write("Anzahl Geschütze?    >> ");
            NumGeschuetze = Helpers.ReadNum();
        }

        public override void ShowChangeOptions()
        {
            base.ShowChangeOptions();
            Console.WriteLine("(A)nzahl Geschütze");
        }

        public override void DoChange()
        {
            base.DoChange();
            if (ToChange == 'A')
            {
                Console.Write("Anzahl Geschütze?    >> ");
                NumGeschuetze = Helpers.ReadNum();
            }
        }

        public override void Show()
        {
            base.Show();
            Console.WriteLine("Anzahl Geschütze: " + NumGeschuetze);
        }
    }
}