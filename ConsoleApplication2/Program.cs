﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
             *  Bitfelder:
             *  man nehme eine Zahl (uint zb)
             *  die Bits können in Konstanten gespeichert werden mit den werten 1 für das letzte Bit, 2 für das vorletzte, dann 4 und so weiter und so weiter.
             *  
             *  Selektives Abfragen:
             *  - Bitfeld & suchbit == suchbit -> true oder false
             *  
             *  Selectives setzen auf true: 
             *  - Bitfeld | suchbit -> Bit wird auf true gesetzt.
             *  
             *  Selektives Setzen auf false:
             *  - Bitfeld = 
             * */

            //IDee: Kann ich das Bit über ne Funktion finden?
            do
            {
                Console.Write("Das wievielste Bit willst Du?");
                int BitPlace = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine(1 << BitPlace);
            } while (Console.ReadLine() == "repeat");

            Console.WriteLine("Enter zum Beenden"); Console.ReadLine();
        }
    }
}
