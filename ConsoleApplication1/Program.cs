﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                int zahl1 = Meine.ReadNum();
                int zahl2 = Meine.ReadNum();
                //string op = Meine.ReadOp();
                Console.Write("Operator? ");
                string op = Console.ReadLine();
                bool OpUnknwon;
                do
                {
                    OpUnknwon = false;
                    switch (op)
                    {
                        case "+": Console.WriteLine(zahl1 + op + zahl2 + "=" + (zahl1 + zahl2)); break;
                        case "-": Console.WriteLine(zahl1 + op + zahl2 + "=" + (zahl1 - zahl2)); break;
                        case "*": Console.WriteLine(zahl1 + op + zahl2 + "=" + (zahl1 * zahl2)); break;
                        case "/": Console.WriteLine(zahl1 + op + zahl2 + "=" + (zahl1 / zahl2)); break;
                        case "%": Console.WriteLine(zahl1 + op + zahl2 + "=" + (zahl1 % zahl2)); break;
                        default:
                            OpUnknwon = true;
                            Console.Write("Den kenn ich nicht... sag mir einen anderen! ");
                            op = Console.ReadLine();
                            break;
                    }
                } while (OpUnknwon);
                Console.Write("Noch eine Berechnung? (j/n)");
            } while (Meine.ReadYesNo() == "j");
        }        
    }

    public static class Meine
    {
        /// <summary>
        /// Liest eine Zahl aus der Konsole, wiederholt die Abfrage im Fehlerfall
        /// </summary>
        /// <returns></returns>
        public static int ReadNum()
        {
            int zahl = 0;

            Console.Write("Eine Zahl bitte: ");

            while (!Int32.TryParse(Console.ReadLine(), out zahl))
            {
                Console.WriteLine("Das war keine Zahl. Noch mal bitte");
            }
            return zahl;
        }

        /// <summary>
        /// Liest einen Operator aus der Konsole, wiederholt wenn der Operator nicht in der Liste ist.
        /// </summary>
        /// <returns></returns>
        public static string ReadOp()
        {
            Console.Write("Den Operator bitte: ");
            string op = Console.ReadLine();
            string ops = "+-*/";
            while (!ops.Contains(op))
            {
                Console.WriteLine("Operator unbekannt.");
                op = Console.ReadLine();
            }
            return op;
        }

        /// <summary>
        /// Fragt explizit nach j oder n
        /// </summary>
        /// <returns></returns>
        public static string ReadYesNo()
        {            
            Console.Write("Noch mal? (j/n)");
            string antw = Console.ReadLine();
            while (antw != "j" && antw != "n")
            {
                Console.Write("Ja was denn nun??? ");
                antw = Console.ReadLine();
            }
            return antw;
        }
    }
}
