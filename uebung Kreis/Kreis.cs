﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Aufgabe:
 * Übergabe Radius im Konstruktor
 * Fläche als Eigenschaft
 * Umfang als Eigenschaft
 * statische Berechnung der Fläche
 * statische Berechnung des Umfangs */

namespace uebung_Kreis
{
    class Kreis
    {
        public double Radius { get; private set; }
        public double Flaeche { get; private set; }
        public double Umfang { get; private set; }

        /// <summary>
        /// Erzeugt einen Kreis mit Radius r
        /// </summary>
        /// <param name="r">Radius</param>
        public Kreis(double r)
        {
            Console.WriteLine("Erzeuge den Kreis...");
            Radius = r;
            Umfang = CalcUmfang(r);
            Flaeche = CalcFlaeche(r);
        }

        /// <summary>
        /// Berechnet einen Kreisumfang mit gegebenem Radius r
        /// </summary>
        /// <param name="r">Radius</param>
        /// <returns>Umfang als double</returns>
        public static double CalcUmfang(double r)
        {
            return 2 * Math.PI * r;
        }

        /// <summary>
        /// Berechnet eine Kreisfläche mit gegebenem Radius r
        /// </summary>
        /// <param name="r">Radius</param>
        /// <returns>Fläche als double</returns>
        public static double CalcFlaeche(double r)
        {
            return Math.PI * Math.Pow(r, 2);
        }
    }
}
