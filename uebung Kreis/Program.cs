﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uebung_Kreis
{
    class Program
    {
        static void Main(string[] args)
        {
            string Trennlinie = "-----------------------------------------";
            do
            {
                Console.WriteLine("Dieses Programm berechnet einen Kreis.\n" + Trennlinie);
                Console.Write("Bitte einen Radius angeben: >> ");
                double UserRadius = Convert.ToDouble(Console.ReadLine());              

                Console.WriteLine(Trennlinie);

                Kreis K = new Kreis(UserRadius);
                Console.WriteLine("Der erzeugte Kreis hat: \n  * Radius "
                                    + UserRadius +
                                  "\n  * Fläche "
                                  + K.Flaeche +
                                  "\n  * Umfang "
                                  + K.Umfang);
                Console.WriteLine(Trennlinie);
                Console.Write("Einzelberechnung von Umfang und Fläche... Bitte einen Radius angeben: >> ");
                UserRadius = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("  * Umfang: " + Kreis.CalcUmfang(UserRadius));
                Console.WriteLine("  * Fläche: " + Kreis.CalcFlaeche(UserRadius));

                Console.WriteLine(Trennlinie);
                Console.Write("x: Programmende     alles andere: weiter >> ");
            } while (Console.ReadLine().ToUpper() != "X");
        }
    }
}
