﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sandbox_console
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] Namen = { "0", "1", "2", "3", "4", "5", "6", "7", "8","9","a","b","c","d","e","f",
                               "g", "h", "i", "j", "k", "l", "m", "n", "m","o","p","q","r","s","t","u" };

            do
            {
                List<string> zwischenschritt = new List<string>();
                int zahl = Meine.ReadNum();
                int basis = Namen.Count();
                while (zahl > 0)
                {
                    zwischenschritt.Add(Namen[zahl % basis]);
                    zahl = zahl - zahl % basis;
                    zahl = zahl / basis;
                }

                for (int i = zwischenschritt.Count - 1; i >= 0; i--)
                {
                    Console.Write(zwischenschritt[i]);
                }
                Console.WriteLine("\r\nNoch eine?");
            } while(Console.ReadLine() =="j");
        }
    }

    public static class Meine
    {
        /// <summary>
        /// Liest eine Zahl aus der Konsole, wiederholt die Abfrage im Fehlerfall
        /// </summary>
        /// <returns></returns>
        public static int ReadNum()
        {
            int zahl = 0;

            Console.Write("Eine Zahl bitte: ");

            while (!Int32.TryParse(Console.ReadLine(), out zahl))
            {
                Console.WriteLine("Das war keine Zahl. Noch mal bitte");
            }
            return zahl;
        }

        /// <summary>
        /// Liest einen Operator aus der Konsole, wiederholt wenn der Operator nicht in der Liste ist.
        /// </summary>
        /// <returns></returns>
        public static string ReadOp()
        {
            Console.Write("Den Operator bitte: ");
            string op = Console.ReadLine();
            string ops = "+-*/";
            while (!ops.Contains(op))
            {
                Console.WriteLine("Operator unbekannt.");
                op = Console.ReadLine();
            }
            return op;
        }

        /// <summary>
        /// Fragt explizit nach j oder n
        /// </summary>
        /// <returns></returns>
        public static string ReadYesNo()
        {
            Console.Write("Noch mal? (j/n)");
            string antw = Console.ReadLine();
            while (antw != "j" && antw != "n")
            {
                Console.Write("Ja was denn nun??? ");
                antw = Console.ReadLine();
            }
            return antw;
        }
    }
}
