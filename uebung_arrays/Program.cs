﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uebung_arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Console.Write("Wie viele Zahlen möchtest Du eingeben? >> ");
                int len = Convert.ToInt32(Console.ReadLine());
                int[] ursprung = new int[len];
                for (int i = 0; i < len; i++)
                {
                    ursprung[i] = Convert.ToInt32(Console.ReadLine());
                }
                Console.Write("\r\nUrsprungs-Array: {");

                Console.Write(string.Join(", ", ursprung)); //string.join entspricht der implode-Funktion in PhP

                Console.Write("}\r\n");

                //ermittel die Längen der Ziel-Arrays
                int len_odd = 0;
                int len_even = 0;

                for (int i = 0; i < len; i++)
                {
                    if (ursprung[i] % 2 == 0)
                    {
                        len_even++;
                    }
                    else
                    {
                        len_odd++;
                    }
                }

                //verteile nun die Werte
                int[] evens = new int[len_even];
                int[] odds = new int[len_odd];

                int numEvens = 0;
                int numOdds = 0;

                for (int i = 0; i < len; i++)
                {
                    int num = ursprung[i];
                    if (num % 2 == 0)
                    {
                        evens[numEvens] = num;
                        numEvens++;
                    }
                    else
                    {
                        odds[numOdds] = num;
                        numOdds++;
                    }
                }

                Console.WriteLine("Die geraden:");
                Console.WriteLine("{" + string.Join(", ", evens) + "}");
                Console.WriteLine("Die ungeraden:");
                Console.WriteLine("{" + string.Join(", ", odds) + "}");
                Console.WriteLine("Für noch einen Versuch schreib j");
            } while (Console.ReadLine().ToUpper() == "J");
        }
    }
}
