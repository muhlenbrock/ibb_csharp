﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace datentypen
{
    class Program
    {
        static void Main(string[] args)
        {
            char Zeichen1 = 'B';
            char Zeichen2 = 'C';

            Console.WriteLine(Zeichen2 - Zeichen1);
            Console.WriteLine((char)(Zeichen2 - Zeichen1));
            Console.WriteLine((char)(Zeichen2 - ('A' - 'a')));

            bool BWert = true;
            for (int i = 0; i<5;i++)
            {
                BWert = BWert ^ true;
                Console.WriteLine("BWert ^ true -> " + BWert);
            }

            Console.WriteLine("true && true ->" + (true && true));

            Console.WriteLine("Ab hier geht es eher um Operatoren...");
            for (int i = 0; i < 10; i++)
            {
                uint start = 1;
                Console.WriteLine((start << i).ToString());
                string drittel =         i < 3  ? "erstes drittel" :
                               i >= 3 && i < 6  ? "zweites drittel" :
                                 "letztes drittel"
            }


            Console.ReadLine();
        }
    }
}
