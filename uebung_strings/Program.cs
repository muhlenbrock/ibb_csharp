﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uebung_strings
{
    class Program
    {
        public const bool MIT_UMLAUT = true;
        //-----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Hauptprogramm (in erster Linie das Menü)
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            string text = "";
            bool ForceContinue = false;
            do
            {
                ForceContinue = false;
                Console.Clear();
                if (text=="") text = FromPrompt("Sag mal einen Text",25);
                Console.WriteLine("Bitte wähle eine Aktion:\n  (1) Umkehren\n  (2) Wörter rückwärts\n  (3) Vokale zählen\n  (4) Nach ASCII Wert sortieren\n  (5) Text ändern");
                string choice = FromPrompt("Auswahl:", 15);
                int i = 0;
                if (!int.TryParse(choice, out i)) i = -1;
                string result = string.Empty;
                switch (i)
                {
                    case 1: result = DrehMichUm(text);  break;
                    case 2: result =DrehDieWorteUm(text); break;
                    case 3: result = " "+WieVieleVokale(text, FromPrompt(" >> Mit Umlauten?", 20).ToUpper() == "J"); break; 
                    case 4: result = SortByASCII(text); break;
                    case 5: ForceContinue = true; text = ""; break;
                    default: Console.WriteLine("Nicht implementiert");break;
                }

                ConsoleBox(result);
            } while (ForceContinue||DoAgain());
            
        }

        //-----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Hilfsfunktion: Stellt eine Frage und liest die Antwort ein (Per Konsole)
        /// </summary>
        /// <param name="s">Frage</param>
        /// <param name="l">Promptlänge (Standard 30)</param>
        /// <returns></returns>
        private static string FromPrompt(string s, int l=30)
        {
            Console.Write(s.PadRight(l - 3, ' ').PadRight(l - 1, '>').PadRight(l, ' '));
            return Console.ReadLine();
        }

        //-----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Wiederholungsfrage
        /// </summary>
        /// <returns></returns>
        private static bool DoAgain()
        {
            string antwort = "";
            do
            {
                Console.WriteLine("Noch mal? (j/n)");
                antwort = Console.ReadLine().ToUpper();
                if (!"JN".Contains(antwort)) Console.Write("Das habe ich nicht verstanden. ");
            } while (!"JN".Contains(antwort));
            
            return antwort == "J";               
        }
        //-----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// dreht einen String um
        /// </summary>
        /// <param name="s">der String</param>
        /// <returns></returns>
        private static string DrehMichUm(string s)
        {
            char[] temp = s.ToCharArray();
            temp = temp.Reverse().ToArray();
            return new string(temp);
        }

        //-----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// zerlegt einen String in Worte und dreht diese um
        /// </summary>
        /// <param name="s">der String</param>
        /// <returns></returns>
        private static string DrehDieWorteUm(string s)
        {
            return string.Join(" ", Explode(" ", s).Reverse());
        }
        //-----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Das gleiche wie string.Split. Nur selbstgemacht-
        /// </summary>
        /// <param name="delimiter">Trennzeichenfolge</param>
        /// <param name="s">der String</param>
        /// <returns></returns>
        private static string[] Explode(string delimiter, string s)
        {
            List<string> items = new List<string>();
            //Delimiter am Anfang und Ende des Strings abschneiden, wenn vorhanden.
            if (s.StartsWith(delimiter)) s = s.Substring(delimiter.Length);
            if (s.EndsWith(delimiter)) s = s.Substring(0, s.Length - delimiter.Length);

            //Und dann zerschnippeln
            int pos = -1;
            do
            {
                pos++;
                pos = s.IndexOf(delimiter);
                if (pos != -1)
                {
                    string sub = s.Substring(0, pos);
                    items.Add(sub);
                    s = s.Substring(pos + delimiter.Length);
                }
                else items.Add(s);
            } while (pos != -1);
            return items.ToArray();
        }

        //-----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Zählt die Vokale in einem String
        /// </summary>
        /// <param name="s">der String</param>
        /// <returns></returns>
        private static int WieVieleVokale(string s,bool uml=false)
        {
            int counter = 0;
            string vokale = uml ? "aeiouäöü" : "aeiou";
            foreach (char c in s)
            {
                if (vokale.Contains(char.ToLower(c))) counter++;
            }
            return counter;
        }

        private static string SortByASCII(string s)
        {
            List<char> Charlist = s.ToList();
            Charlist.Sort();
            return new string(Charlist.ToArray());
        }

        private static void ConsoleBox(string s)
        {
            string horizontal_line = new string('-', s.Length + 2).PadLeft(s.Length+3,'+').PadRight(s.Length+4,'+');
            string[] splitted = Explode("\n", s);
            Console.WriteLine(horizontal_line + "\n| " + s + " |\n" + horizontal_line);
        }
    }
}
