﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValueRef
{
    class Program
    {
        static void Main(string[] args)
        {
            int Zahl1 = 3;
            int Zahl2 = 4;

            Zahl2 = Zahl1;
            Console.WriteLine(Zahl2);
            Zahl1 = 4711;
            Console.WriteLine(Zahl2);

            Beispiel b1 = new Beispiel();
            Beispiel b2 = new Beispiel();

            b1.Wert = 3;
            b2.Wert = 4;

            b1 = b2;
            Console.WriteLine(b1.Wert);
            b2.Wert = 4711;
            Console.WriteLine(b1.Wert);
            

            BeispielMethoden bm = new BeispielMethoden();
            bm.Rein(Zahl1);
            Console.WriteLine(Zahl1);

            bm.ReinObjekt(b1);
            Console.WriteLine(b1.Wert);

            Console.ReadLine();
        }

        class Beispiel
        {
            public int Wert { get; set; }
        }

        class BeispielMethoden
        {
            public void Rein(int i)
            {
                i = 666;
            }

            public void ReinObjekt(Beispiel bsp)
            {
                bsp.Wert = 666;
                
            }
        }
    }
}
