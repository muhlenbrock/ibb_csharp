﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace @static
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = Demo.Multi(2, 3);
            Point v = new Point(2, 4);
            Point w = new Point(4, 8);
            int j = Demo.Multi(v, w);
        }

        class Demo
        {
            /// <summary>
            /// Produkt von zwei Zahlen
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <returns></returns>
            public static int Multi(int x, int y)
            { return x * y; }

            /// <summary>
            /// Skalarprodukt von zwei Vektoren
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <returns></returns>
            public static int Multi(Point x, Point y)
            {
                return x.X * y.X + x.Y * y.Y;
            }
        }

    }
}
